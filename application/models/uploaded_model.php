<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Uploaded_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('session');
        $this->load->library('encrypt');
    }
    
    ///----01
    function nsc01_count()
    {
        $run = $this->db->query("SELECT * FROM source S
            INNER JOIN cabang AS C ON C.id_cabang=S.id_cabang
            WHERE S.tipe='nsc01'");
        return $run->num_rows();
    }
    
    function nsc01_data($start, $limit)
    {
        $run = $this->db->query("SELECT * FROM source S
            INNER JOIN cabang AS C ON C.id_cabang=S.id_cabang
            WHERE S.tipe='nsc01'
            LIMIT $start, $limit");
        return $run;
    }
    
    
    ///----08
    function nsc08_count()
    {
        $run = $this->db->query("SELECT * FROM source S
            INNER JOIN cabang AS C ON C.id_cabang=S.id_cabang
            WHERE S.tipe='nsc08'");
        return $run->num_rows();
    }
    
    function nsc08_data($start, $limit)
    {
        $run = $this->db->query("SELECT * FROM source S
            INNER JOIN cabang AS C ON C.id_cabang=S.id_cabang
            WHERE S.tipe='nsc08'
            LIMIT $start, $limit");
        return $run;
    }
    
    ///----09
    function nsc09_count()
    {
        $run = $this->db->query("SELECT * FROM source S
            INNER JOIN cabang AS C ON C.id_cabang=S.id_cabang
            WHERE S.tipe='nsc09'");
        return $run->num_rows();
    }
    
    function nsc09_data($start, $limit)
    {
        $run = $this->db->query("SELECT * FROM source S
            INNER JOIN cabang AS C ON C.id_cabang=S.id_cabang
            WHERE S.tipe='nsc09'
            LIMIT $start, $limit");
        return $run;
    }
        
    function hapus_data($id_source, $tipe)
    {
        $run = $this->db->query("DELETE S , N  FROM source AS S INNER JOIN $tipe N  
            WHERE S.id_source= N.id_source AND N.id_source=$id_source");
        
        return $this->db->affected_rows();
    }
}
?>