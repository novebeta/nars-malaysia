<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('session');
        $this->load->library('encrypt');
    }
    
    function get_id_cabang($kode_cabang)
    {
        $kode_cabang = $this->db->escape($kode_cabang);
        $run = $this->db->query("SELECT id_cabang FROM cabang WHERE kode_cabang=$kode_cabang LIMIT 1");
        $data = $run->row();
        
        return $data->id_cabang;
    }
    
    function insert_data_kembali($nobase, $kodebrg, $qty, $tanggal, $id_source, $id_cabang, $ket)
    {
        $nobase = $this->db->escape($nobase);
        $kodebrg = $this->db->escape($kodebrg);
        $tanggal = $this->db->escape($tanggal);
        $ket = $this->db->escape($ket);
        
        $run = $this->db->query("SELECT * FROM transaksi 
            WHERE nobase=$nobase AND kodebrg=$kodebrg AND tanggal=$tanggal AND id_cabang=$id_cabang LIMIT 1");
        if ($run->num_rows() <= 0)
        {
            $this->db->query("INSERT INTO transaksi (nobase, nofak, kodebrg, qty, tanggal, id_source, id_cabang, data_kembali, keterangan)
                VALUES ($nobase, '-', $kodebrg, $qty, $tanggal, $id_source, $id_cabang, '1', $ket)");
        }
    }
    
    function insert_pasien($dataarray)
    {
        $this->db->insert_batch('pasien', $dataarray);
    }
    
    function gabung()
    {
        $array = Array();
        $run = $this->db->query("INSERT INTO transaksi (nobase, nofak, dokter, kodebrg, satuan, qty, harga, discount, tanggal, id_source, id_cabang) 
        SELECT n1.nobase, n9.nofak, n9.dokter, n9.kodebrg, n9.satuan, n9.qty, n9.harga, n9.discount, n9.tanggal, n9.id_source, n9.id_cabang  FROM nsc09 AS n9
            INNER JOIN nsc08 AS n8 ON n8.nofak=n9.nofak
            INNER JOIN nsc01 AS n1 ON n1.nokartu=n8.nokartu
            GROUP BY n9.id_nsc09");
        
        //$i=0;
        //foreach($run->result() as $row)
//        {
//            $array[$i]['nobase'] = $row->nobase;
//            $array[$i]['nofak'] = $row->nofak;
//            $array[$i]['dokter'] = $row->dokter;
//            $array[$i]['kodebrg'] = $row->kodebrg;
//            $array[$i]['qty'] = $row->qty;
//            $array[$i]['satuan'] = $row->satuan;
//            $array[$i]['harga'] = $row->harga;
//            $array[$i]['discount'] = $row->discount;
//            $array[$i]['tanggal'] = $row->tanggal;
//            $array[$i]['id_source'] = $row->id_source;
//            $array[$i]['id_cabang'] = $row->id_cabang;
//            $i++;
//        }
//        
//        $this->db->insert_batch('transaksi', $array);
        $this->db->query("TRUNCATE TABLE nsc09");
        $this->db->query("TRUNCATE TABLE nsc08");
        //$this->db->query("TRUNCATE TABLE nsc01");
        return 1;
    }
    
    function cek_nobase($nobase)
    {
        $nobase = $this->db->escape($nobase);
        //$nokartu = $this->db->escape($nokartu);
        
        $run = $this->db->query("SELECT id_nsc01 FROM nsc01 WHERE nobase=$nobase");
        
        return $run->num_rows();
    }
    
    function cek_pasien($nobase)
    {
        $nobase = $this->db->escape($nobase);
        //$nokartu = $this->db->escape($nokartu);
        
        $run = $this->db->query("SELECT id_pasien FROM pasien WHERE nobase=$nobase");
        
        return $run->num_rows();
    }
    
    function update_noax($nobase, $noax)
    {
        $nobase = $this->db->escape($nobase);
        $noax = $this->db->escape($noax);
        
        $run = $this->db->query("UPDATE pasien SET noax=$noax WHERE nobase=$nobase");
        
        return $this->db->affected_rows();
    }
    
    function update_noax_detail($nobase, $noax, $alamat, $tgllh, $telp, $gender)
    {
        $nobase = $this->db->escape($nobase);
        $noax = $this->db->escape($noax);
        $alamat = $this->db->escape($alamat);
        $tgllh = $this->db->escape($tgllh);
        $telp = $this->db->escape($telp);
        $gender = $this->db->escape($gender);
        
        $run = $this->db->query("UPDATE pasien SET noax=$noax, alamat=$alamat, tgllh=$tgllh, telp=$telp, sex=$gender WHERE nobase=$nobase");
        
        return $this->db->affected_rows();
    }

    function insert_pasien_ax($dataarray)
    {
        $this->db->insert_batch('pasien', $dataarray);
        //$nobase = $this->db->escape($nobase);
//        $noax = $this->db->escape($noax);
//        $nama = $this->db->escape($nama);
//        $phone = $this->db->escape($phone);
//        $address = $this->db->escape($address);
//        
//        
//        $run = $this->db->query("INSERT INTO pasien (nobase, noax, namacus, telp, alamat, id_source)
//            VALUES ($nobase, $noax, $nama, $phone, $address, $id_source)");
//        
//        return $this->db->insert_id();
    }
    
    function insert_transaksi_ax($dataarray)
    {
        $this->db->insert_batch('transaksi', $dataarray);
    }
    
    function cek_nofaktur($nofak, $cabang, $tanggal)
    {
        $nofak = $this->db->escape($nofak);
        $tanggal = $this->db->escape($tanggal);
        
        $run = $this->db->query("SELECT * FROM transaksi AS T
            WHERE T.nofak=$nofak AND T.id_cabang=$cabang AND DATE(T.tanggal)=DATE($tanggal) 
            LIMIT 1");
        //$run = $this->db->query("SELECT * FROM nsc08 AS n8
//            INNER JOIN source AS S ON S.id_source=n8.id_source
//            WHERE n8.nofak=$nofak AND S.id_cabang=$cabang LIMIT 1");
        
        return $run->num_rows();
    }
    
    function get_cabang()
    {
        $run = $this->db->query("SELECT * FROM cabang");
        $data = '';
        
        foreach($run->result() as $row)
        {
            $data .= '<option value="'.$row->id_cabang.'">'.$row->kode_cabang.'</option>';
        }
        
        return $data;
    }
    
    function add_source($source, $tipe, $cabang)
    {
        $source = $this->db->escape($source);
        $tipe = $this->db->escape($tipe);
        
        $run = $this->db->query("INSERT INTO source (tipe, source, id_cabang, tanggal)
            VALUES ($tipe, $source, $cabang, NOW())");
        
        return $this->db->insert_id();
    }
    
    function add_row($dataarray, $tabel)
    {
        $this->db->insert_batch($tabel, $dataarray);
    }
    
    function add_pasien($dataarray)
    {
        $this->db->insert_batch('pasien', $dataarray);
    }

    function update_tanggal_backup($cabang)
    {
        $run = $this->db->query("SELECT tanggal_backup FROM cabang WHERE id_cabang=$cabang");
        $row = $run->row();
        $tanggal = date("Y-m-d", strtotime("+1 day", strtotime($row->tanggal_backup)));
        
        $run = $this->db->query("UPDATE cabang SET tanggal_backup='$tanggal' WHERE id_cabang=$cabang");
    }
}
?>
