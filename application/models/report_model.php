<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('session');
        $this->load->library('encrypt');
    }
    
	function get_kode_cbg($cabang)
	{

	$cbg = $this->db->query("SELECT kode_cabang FROM cabang WHERE id_cabang=$cabang");
	$row_cbg = $cbg->row();
	return $row_cbg->kode_cabang;

	}

    function get_perawatan($awal, $akhir, $cabang)
    {
        $awal = $this->db->escape($awal);
        $akhir = $this->db->escape($akhir);
        
        $data = $this->db->query("SELECT T.*, P.namacus FROM transaksi AS T
            INNER JOIN pasien AS P ON P.nobase=T.nobase
            WHERE DATE(T.tanggal)>=DATE($awal) AND DATE(T.tanggal)<=DATE($akhir) AND (LCASE(T.satuan)='unit' OR LCASE(satuan)='org') AND T.id_cabang=$cabang");
        
        return $data;
    }
    
    function get_pasien_baru($awal, $akhir, $cabang)
    {
        $awal = $this->db->escape($awal);
        $akhir = $this->db->escape($akhir);
        
        $data = $this->db->query("SELECT * FROM pasien 
            WHERE DATE(dtgl)>=DATE($awal) AND DATE(dtgl)<=DATE($akhir) AND LCASE(RIGHT(nobase, 5))=LCASE('$cabang')");
        
        return $data;
    }
    
    function pasien_riil($awal, $akhir, $cabang)
    {
        $awal = $this->db->escape($awal);
        $akhir = $this->db->escape($akhir);
        
        $data = $this->db->query("SELECT T.*, P.namacus FROM transaksi AS T
            INNER JOIN pasien AS P ON P.nobase=T.nobase
            WHERE DATE(T.tanggal)>=DATE($awal) AND DATE(T.tanggal)<=DATE($akhir) AND T.id_cabang=$cabang
            GROUP BY T.nobase
            ORDER BY T.tanggal ASC");
        
        return $data;
    }
}
?>
