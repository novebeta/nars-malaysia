<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('session');
        $this->load->library('encrypt');
    }

    
    function cek_masuk($user, $pwd)
    {
        $user = $this->db->escape($user);
        
        $run = $this->db->query("SELECT * FROM user WHERE user_name=$user LIMIT 1");
        
        if ($run->num_rows())
        {
            $row_user = $run->row();
            $pwd_user = $this->encrypt->decode($row_user->password);
            if ($pwd != $pwd_user)
            {
                $this->session->sess_destroy();
                return false;
            }
            else
            {
                $now = time();
                $expired = $now + waktu_login;
                $login = array('id_user' => $row_user->id_user,
                    'nama' => $row_user->nama,
                    'level' => $row_user->level,
                    'cabang' => $row_user->id_cabang,
                    'expired' => $expired);
                $this->session->set_userdata($login);
                return true;
            }
            
        }
        else
        {
            $this->session->sess_destroy();
            return false;
        }
    }
    
}
?>
