<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Summary_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('session');
        $this->load->library('encrypt');
    }
    
    function find_data($nama, $alamat, $telp, $no_base, $f, $start, $limit)
    {
        $where = "";
        $start = empty($start)? 0:$start;
        if (!empty($nama))
        {
            if ($f == "f")
                $nama = $this->db->escape("%".$nama."%");
            else
                $nama = $this->db->escape($nama."%");
            $where .= " WHERE namacus LIKE $nama";
        }
        
        if (!empty($alamat))
        {
            $alamat = $this->db->escape("%".$alamat."%");
            if (!empty($where))
                $where .= " AND alamat LIKE $alamat";
            else
                $where .= " WHERE alamat LIKE $alamat";
        }
        
        if (!empty($telp))
        {
            $telp = $this->db->escape("%".$telp."%");
            if (!empty($where))
                $where .= " AND telp LIKE $telp";
            else
                $where .= " WHERE telp LIKE $telp";
        }
        
        if (!empty($no_base))
        {
        $no_base = $this->db->escape($no_base);
            if (!empty($where))
                $where .= " AND nobase = $no_base";
            else
                $where .= " WHERE nobase = $no_base";
        }
        
            
        $run = $this->db->query("SELECT * FROM pasien
            $where
            GROUP BY nobase
            ORDER BY namacus
            LIMIT $start, $limit");
        
        return $run;
    }
    
    
    
    function find_count($nama, $alamat, $telp, $no_base, $f)
    {
        $where = "";
        
        if (!empty($nama))
        {
            if ($f == "f")
                $nama = $this->db->escape("%".$nama."%");
            else
                $nama = $this->db->escape($nama."%");
            $where .= " WHERE namacus LIKE $nama";
        }
        
        if (!empty($alamat))
        {
            $alamat = $this->db->escape("%".$alamat."%");
            if (!empty($where))
                $where .= " AND alamat LIKE $alamat";
            else
                $where .= " WHERE alamat LIKE $alamat";
        }
        
        if (!empty($telp))
        {
            $telp = $this->db->escape("%".$telp."%");
            if (!empty($where))
                $where .= " AND telp LIKE $telp";
            else
                $where .= " WHERE telp LIKE $telp";
        }
        
        if (!empty($no_base))
        {
        $no_base = $this->db->escape($no_base);
            if (!empty($where))
                $where .= " AND nobase = $no_base";
            else
                $where .= " WHERE nobase = $no_base";
        }
        
            
        $run = $this->db->query("SELECT SUM(1) AS cnt FROM pasien
            $where");
        $row = $run->row();
        return $row->cnt;
    }
    
    
    
    
    function detail_pasien_all($nobase)
    {
        $nobase = $this->db->escape($nobase);
        //$kartu = $this->db->escape($kartu);
        $run = $this->db->query("SELECT T.nofak, T.kodebrg, T.satuan, T.tanggal, T.keterangan, T.keterangan_resep , SUM(T.qty) AS qty, C.kode_cabang FROM transaksi AS T
            LEFT JOIN cabang AS C ON C.id_cabang=T.id_cabang
            WHERE T.nobase=$nobase
			GROUP BY T.nofak, T.nobase, T.kodebrg
			HAVING qty > 0
            ORDER BY T.tanggal DESC, T.nofak ASC");
        //$run = $this->db->query("SELECT T.*, C.kode_cabang FROM transaksi AS T
//            LEFT JOIN source AS S ON S.id_source=T.id_source
//            LEFT JOIN cabang AS C ON C.id_cabang=S.id_cabang
//            WHERE T.nobase=$nobase
//            ORDER BY T.tanggal DESC, T.nofak ASC");
        
        /*$run = $this->db->query("SELECT n1.namacus, n1.nobase, n9.*, C.kode_cabang FROM nsc09 AS n9
            INNER JOIN nsc08 AS n8 ON n8.nofak=n9.nofak
            INNER JOIN nsc01 AS n1 ON n1.nokartu=n8.nokartu
            LEFT JOIN source AS S ON S.id_source=n9.id_source
            LEFT JOIN cabang AS C ON C.id_cabang=S.id_cabang
            WHERE n1.nobase=$nobase
            ORDER BY n9.tanggal DESC, n9.nofak DESC");*/
        
        return $run;
    }

    function info_pasien($nobase)
    {
        $nobase = $this->db->escape($nobase);
        //$kartu = $this->db->escape($kartu);
        
        $run = $this->db->query("SELECT * FROM pasien WHERE nobase=$nobase");
        
        return $run->row();
    }
}
?>
