<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Backup_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('session');
        $this->load->library('encrypt');
    }

    
    function ambil_tanggal($cabang)
    {
        $cabang = $this->db->escape($cabang);
        
        $run = $this->db->query("SELECT tanggal_backup FROM cabang WHERE kode_cabang=$cabang LIMIT 1");
        
        $row = $run->row();
        return $row->tanggal_backup;
    }
    
    function buat_backup_pasien($arr_pasien)
    {
        $this->db->trans_begin();
        
        $this->db->insert_batch('pasien', $arr_pasien);
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return '0'.$this->db->_error_message();
        }
        else
        {
            $this->db->trans_commit();
            return 1;
        }
    }
    
    function buat_backup_transaksi($arr_transaksi, $cabang)//($nobase, $resep, $kodebrg, $satuan, $qty, $tanggal, $cabang, $komen, $komen_resep)
    {
        $this->db->trans_begin();
        
        $this->db->insert_batch('transaksi', $arr_transaksi);
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return '0'.$this->db->_error_message();
        }
        else
        {
            $this->db->trans_commit();
            
            
            return 1;
        }
        
        //$nobase = $this->db->escape($nobase);
//        $kodebrg = $this->db->escape($kodebrg);
//        $satuan = $this->db->escape($satuan);
//        $resep = $this->db->escape($resep);
//        $tanggal = $this->db->escape($tanggal);
//        $komen = $this->db->escape($komen);
//        $komen_resep = $this->db->escape($komen_resep);
//                
//        if ($cabang > 0)
//        {
//            $run = $this->db->query("INSERT INTO transaksi (nobase, nofak, kodebrg, satuan, qty, tanggal, id_cabang, keterangan, keterangan_resep)
//                VALUES ($nobase, $resep, $kodebrg, $satuan, $qty, $tanggal, $cabang, $komen, $komen_resep)");
//            
//            return $this->db->insert_id();
//        }
//        else
//            return "0";
    }
    
    
    function cek_pasien($nobase)
    {
        $nobase = $this->db->escape($nobase);
        
        $run = $this->db->query("SELECT * FROM pasien WHERE nobase=$nobase");
        
        return $run->num_rows();
    }
    
    function update_tanggal_backup($cabang)
    {
        $run = $this->db->query("SELECT tanggal_backup FROM cabang WHERE id_cabang=$cabang");
        $row = $run->row();
        $tanggal = date("Y-m-d", strtotime("+1 day", strtotime($row->tanggal_backup)));
        
        $run = $this->db->query("UPDATE cabang SET tanggal_backup='$tanggal' WHERE id_cabang=$cabang");
        
            
        return $this->db->affected_rows();
    }
}
?>
