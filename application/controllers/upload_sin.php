<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_sin extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('upload_model');
        $this->load->library('session');
        $this->load->library('Encrypt');
        
        $this->load->library('login_cek');
        $this->login_cek->is_login();
    }


    function index()
    {
        $data['msg'] = '';
        $data['cabang'] = $this->upload_model->get_cabang();
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('upload_view', $data);
        $this->load->view('footer');
    }

//------------------------------------------------------------------

    function upload_file()
    {
        $tipe = $this->input->post('tipe');
        $cabang = $this->input->post('cabang');
        
        $data['cabang'] = $this->upload_model->get_cabang();
        $config['upload_path'] = upload_path;
		$config['allowed_types'] = '*';
		$config['max_size']	= '102400';
        $config['overwrite'] = TRUE;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$data['msg'] = $this->upload->display_errors();
		}
		else
		{
			$info =  $this->upload->data();
            ini_set("memory_limit","5120M");
            $error = 0;
            $upload_data = $this->upload->data();
            $file =  $upload_data['full_path'];
            error_reporting(E_ALL ^ E_NOTICE);
            $dataexcel = Array();
            $id_source = $this->upload_model->add_source($info['full_path'], $tipe, $cabang);
            
            $this->load->library('excel');
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);
            
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            
            
            
            if ($id_source > 0)
            {
                switch($tipe)
                {
                    case 'nsc01':
                        $match = strpos($sheet->getCellByColumnAndRow(0, 1)->getValue(), "NOKARTU");
                        if ($match !== false)//true
                        {
                            for ($i = 2; $i <= $highestRow; $i++) 
                            {
                                $nobase = $sheet->getCellByColumnAndRow(0, 1)->getValue();
                                $namacus = $sheet->getCellByColumnAndRow(1, 1)->getValue();
                                
                                if ($this->upload_model->cek_nobase($nobase) <= 0 || !empty($nobase) || !empty($namacus))
                                {
                                    
                                    $dataexcel[$i]['nokartu'] = $sheet->getCellByColumnAndRow(0, $i)->getValue();
                                    $dataexcel[$i]['nobase'] = $sheet->getCellByColumnAndRow(1, $i)->getValue();
                                    $dataexcel[$i]['namacus'] = $sheet->getCellByColumnAndRow(2, $i)->getValue();
                                    $dataexcel[$i]['namadep'] = $sheet->getCellByColumnAndRow(3, $i)->getValue();
                                    $dataexcel[$i]['alamat'] = $sheet->getCellByColumnAndRow(4, $i)->getValue();
                                    $dataexcel[$i]['kota'] = $sheet->getCellByColumnAndRow(5, $i)->getValue();
                                    $dataexcel[$i]['telp'] = $sheet->getCellByColumnAndRow(6, $i)->getValue();
                                    $dataexcel[$i]['jnscus'] = $sheet->getCellByColumnAndRow(7, $i)->getValue();
                                    $dataexcel[$i]['tgllh'] =  PHPExcel_Style_NumberFormat::toFormattedString($sheet->getCellByColumnAndRow(8, $i)->getValue(), "YYYY-MM-DD");
                                    $dataexcel[$i]['sex'] = $sheet->getCellByColumnAndRow(9, $i)->getValue();
                                    $dataexcel[$i]['usia'] = $sheet->getCellByColumnAndRow(10, $i)->getValue();
                                    $dataexcel[$i]['kerja'] = $sheet->getCellByColumnAndRow(11, $i)->getValue();
                                    $dataexcel[$i]['awal'] = $sheet->getCellByColumnAndRow(12, $i)->getValue();
                                    $dataexcel[$i]['akhir'] = $sheet->getCellByColumnAndRow(13, $i)->getValue();
                                    $dataexcel[$i]['akhirke'] = $sheet->getCellByColumnAndRow(14, $i)->getValue();
                                    $dataexcel[$i]['saldo'] = $sheet->getCellByColumnAndRow(15, $i)->getValue();
                                    $dataexcel[$i]['dtgl'] = $sheet->getCellByColumnAndRow(16, $i)->getValue();
                                    $dataexcel[$i]['jmlcp'] = $sheet->getCellByColumnAndRow(17, $i)->getValue();
                                    $dataexcel[$i]['id_source'] = $id_source;
                                }
                            }
                        }
                        else
                        {
                            $data['msg'] = '<strong>File yang anda masukkan bukan NSC01</strong>';
                            $error = 1;
                        }
                    break;
                    
                    case 'nsc08':
                        $match = strpos($sheet->getCellByColumnAndRow(1, 1)->getValue(), "NOFAK");
                        if ($match !== false)//true
                        {
                            for ($i = 2; $i <= $highestRow; $i++) 
                            {
                                $dataexcel[$i]['tanggal'] =  PHPExcel_Style_NumberFormat::toFormattedString($sheet->getCellByColumnAndRow(0, $i)->getValue(), "YYYY-MM-DD");
                                $dataexcel[$i]['nofak'] = $sheet->getCellByColumnAndRow(1, $i)->getValue();
                                $dataexcel[$i]['nokartu'] = $sheet->getCellByColumnAndRow(2, $i)->getValue();
                                /*$dataexcel[$i]['kodekar'] = $dataread['cells'][$i][4];
                                $dataexcel[$i]['card'] = $dataread['cells'][$i][5];
                                $dataexcel[$i]['jumlah'] = $dataread['cells'][$i][6];
                                $dataexcel[$i]['discount'] = $dataread['cells'][$i][7];
                                $dataexcel[$i]['ppn'] = $dataread['cells'][$i][8];
                                $dataexcel[$i]['potong'] = $dataread['cells'][$i][9];
                                $dataexcel[$i]['bayar'] = $dataread['cells'][$i][10];
                                $dataexcel[$i]['lunas'] = $dataread['cells'][$i][11];
                                $dataexcel[$i]['tgllunas'] =  $this->convert_date($dataread['cells'][$i][12]);
                                $dataexcel[$i]['ket'] = $dataread['cells'][$i][13];
                                $dataexcel[$i]['tutup'] = $dataread['cells'][$i][14];*/
                                $dataexcel[$i]['id_source'] = $id_source;
                            }
                        }
                        else
                        {
                            $data['msg'] = '<strong>File yang anda masukkan bukan NSC08</strong>';
                            $error = 1;
                        }
                    break;
                    
                    case 'nsc09':
                        $match = strpos($sheet->getCellByColumnAndRow(3, 1)->getValue(), "KODEBRG");
                        if ($match !== false)//true
                        {
                            for ($i = 2; $i <= $highestRow; $i++) 
                            {
                                if ($this->upload_model->cek_nofaktur($sheet->getCellByColumnAndRow(1, $i)->getValue(), $cabang) <= 0)
                                {
                                    $dataexcel[$i]['tanggal'] = PHPExcel_Style_NumberFormat::toFormattedString($sheet->getCellByColumnAndRow(0, $i)->getValue(), "YYYY-MM-DD");
                                    $dataexcel[$i]['nofak'] = $sheet->getCellByColumnAndRow(1, $i)->getValue();
                                    $dataexcel[$i]['dokter'] = $sheet->getCellByColumnAndRow(2, $i)->getValue();
                                    $dataexcel[$i]['kodebrg'] = $sheet->getCellByColumnAndRow(3, $i)->getValue();
                                    $dataexcel[$i]['jasa'] = $sheet->getCellByColumnAndRow(4, $i)->getValue();
                                    $dataexcel[$i]['jsdr'] = $sheet->getCellByColumnAndRow(5, $i)->getValue();
                                    $dataexcel[$i]['qty'] = $sheet->getCellByColumnAndRow(6, $i)->getValue();
                                    $dataexcel[$i]['satuan'] = $sheet->getCellByColumnAndRow(7, $i)->getValue();
                                    $dataexcel[$i]['harga'] = $sheet->getCellByColumnAndRow(8, $i)->getValue();
                                    $dataexcel[$i]['discount'] = $sheet->getCellByColumnAndRow(9, $i)->getValue();
                                    $dataexcel[$i]['id_source'] = $id_source;
                                }
                            }
                        }
                        else
                        {
                            $data['msg'] = '<strong>File yang anda masukkan bukan NSC09</strong>';
                            $error = 1;
                        }
                    break;
                }
            }
            else
            {
                $data['msg'] = 'Terjadi error saat membuat header data';
                $error = 1;
            }
            
            if ($error == 0)
            {
                $this->upload_model->add_row($dataexcel, $tipe);
                $data['msg'] = 'Prosess Upload telah selesai';
            }
            flush();
		}
        
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('upload_view', $data);
        $this->load->view('footer');
    }
    
    function convert_date($tanggal)
    {
        $part = explode("/", $tanggal);
        
        if (count($part) == 3)
        {
            $date = $part[2]."-".$part[0]."-".$part[1];
            return date("Y-m-d", strtotime($date));
        }
        else
        {
            $date = '';
            return $date;
        }
            
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    function upload_ajax()
    {
        $data = $this->input->post('data');
        $id_source = $this->input->post('id_source');
        $tipe = $this->input->post('tipe');
        $cabang = $this->input->post('cabang');
        $reject = "";
        
        $row_data = Array();
        switch($tipe)
        {
            case 'nsc01':
                $row_pasien = Array();
                $index = 0;
                for($i = 0; $i <= count($data)-1; $i++)
                {
                    if (!empty($data[$i]) && !empty($id_source))
                    {
                        $item = explode("^", $data[$i]);
                        //$pasien_exist = $this->upload_model->cek_nobase($item[1]);
                        
                        
                        if (!empty($item[0]))
                        {
                            $row_data[$i]['nokartu'] = $item[0];
                            $row_data[$i]['nobase'] = $item[1];
                            $row_data[$i]['namacus'] = $item[2];
                            $row_data[$i]['namadep'] = $item[3];
                            $row_data[$i]['alamat'] = $item[4];
                            $row_data[$i]['kota'] = $item[5];
                            $row_data[$i]['telp'] = $item[6];
                            $row_data[$i]['jnscus'] = $item[7];
                            $row_data[$i]['tgllh'] = $this->convert_date($item[8]);
                            $row_data[$i]['sex'] = $item[9];
                            //$row_data[$i]['usia'] = $item[10];
                            $row_data[$i]['kerja'] = $item[11];
                            $row_data[$i]['awal'] = $this->convert_date($item[12]);
                            $row_data[$i]['akhir'] = $this->convert_date($item[13]);
                            $row_data[$i]['akhirke'] = $item[14];
                            $row_data[$i]['saldo'] = $item[15];
                            $row_data[$i]['dtgl'] = $this->convert_date($item[16]);
                            //$row_data[$i]['jmlcp'] = $item[17];
                            $row_data[$i]['id_source'] = $id_source;
                            
                            
                            $pasien_exist = $this->upload_model->cek_pasien($item[1]);
                            if ($pasien_exist <= 0)
                            {
                                $row_pasien[$index]['nokartu'] = $item[0];
                                $row_pasien[$index]['nobase'] = $item[1];
                                $row_pasien[$index]['namacus'] = $item[2];
                                $row_pasien[$index]['namadep'] = $item[3];
                                $row_pasien[$index]['alamat'] = $item[4];
                                $row_pasien[$index]['kota'] = $item[5];
                                $row_pasien[$index]['telp'] = $item[6];
                                $row_pasien[$index]['jnscus'] = $item[7];
                                $row_pasien[$index]['tgllh'] = $this->convert_date($item[8]);
                                $row_pasien[$index]['sex'] = $item[9];
                                //$row_pasien[$index]['usia'] = $item[10];
                                $row_pasien[$index]['kerja'] = $item[11];
                                $row_pasien[$index]['awal'] = $this->convert_date($item[12]);
                                $row_pasien[$index]['akhir'] = $this->convert_date($item[13]);
                                $row_pasien[$index]['akhirke'] = $item[14];
                                $row_pasien[$index]['saldo'] = $item[15];
                                $row_pasien[$index]['dtgl'] = $this->convert_date($item[16]);
                                //$row_pasien[$index]['jmlcp'] = $item[17];
                                $row_pasien[$index]['id_source'] = $id_source;
                                $index++;
                            }
                            else
                            {
                                $reject .= "- KARTU:".$item[0]." -NO_BASE:".$item[1]." [".$item[2]."]<br />";
                            }
                        }
                    }
                }
                
                if (count($row_pasien) > 0)
                    $this->upload_model->add_pasien($row_pasien);
            break;
            
            case 'nsc08':
                for($i = 0; $i <= count($data)-1; $i++)
                {
                    if (!empty($data[$i]) && !empty($id_source))
                    {
                        $item = explode("^", $data[$i]);
                        $faktur_exist = $this->upload_model->cek_nofaktur($item[1], $cabang, $this->convert_date($item[0]));
                        
                        if ($faktur_exist <= 0 && $item[1] != "")
                        {
                            $row_data[$i]['tanggal'] = $this->convert_date($item[0]);
                            $row_data[$i]['nofak'] = $item[1];
                            $row_data[$i]['nokartu'] = $item[2];
                            //$row_data[$i]['kodebrg'] = $item[3];
//                            $row_data[$i]['jasa'] = $item[4];
//                            $row_data[$i]['jsdr'] = $item[5];
//                            $row_data[$i]['qty'] = $item[6];
//                            $row_data[$i]['satuan'] = $item[7];
//                            $row_data[$i]['harga'] = $item[8];
//                            $row_data[$i]['discount'] = $item[9];
                            $row_data[$i]['id_source'] = $id_source;
                        }
                    }
                }
            break;
            
            case 'nsc09':
                for($i = 0; $i <= count($data)-1; $i++)
                {
                    if (!empty($data[$i]) && !empty($id_source))
                    {
                        $item = explode("^", $data[$i]);
//                        $faktur_exist = $this->upload_model->cek_nofaktur($item[1], $cabang);
//                        
                        if ($item[1] != "")
                        {
                            $row_data[$i]['tanggal'] = $this->convert_date($item[0]);
                            $row_data[$i]['nofak'] = $item[1];
                            $row_data[$i]['dokter'] = $item[2];
                            $row_data[$i]['kodebrg'] = $item[3];
                            $row_data[$i]['jasa'] = $item[4];
                            $row_data[$i]['jsdr'] = $item[5];
                            $row_data[$i]['qty'] = $item[6];
                            $row_data[$i]['satuan'] = $item[7];
                            $row_data[$i]['harga'] = $item[8];
                            $row_data[$i]['discount'] = $item[9];
                            $row_data[$i]['id_source'] = $id_source;
                            $row_data[$i]['id_cabang'] = $cabang;
                        }
                    }
                }
            break;
        }
        
        if (count($row_data) > 0)
            $this->upload_model->add_row($row_data, $tipe, $id_source);
        
        
        echo $reject;
    }
    
    function create_source()
    {
        $tipe = $this->input->post('tipe');
        $path = $this->input->post('path');
        $cabang = $this->input->post('cabang')? $this->input->post('cabang'):0;
        
        $data = $this->upload_model->add_source($path, $tipe, $cabang);
        echo $data;
    }
    
    function combine()
    {
        $data = $this->upload_model->gabung();
        
        echo $data;
    }
}
