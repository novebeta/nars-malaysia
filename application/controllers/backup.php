<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backup extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('backup_model');
        $this->load->model('upload_model');
        
        $this->load->library('xmlrpc');
		$this->load->library('xmlrpcs');
    }
    
    function history_pasien($request)
    {
        $parameters = $request->output_parameters();

		$response = array(
			array(
					'you_said'  => $parameters['0'],
					'i_respond' => 'Not bad at all.'),
			'struct');

        
        $data = $parameters['0'];//gzuncompress($parameters['0']);
        $cabang = $parameters['1'];
        
        $data = json_decode($data);
        $ret = '';
        foreach($data as $row)
        {
            if ($row->nama != "")
            {
                $part = explode(" ", $row->nama);
                $pos = count($part)-1;
                $nobase = $part[$pos];
                
                $nama_pasien = "";                            
                if ($pos > 1)
                {
                    for($x=0; $x<=$pos-1; $x++)
                        $nama_pasien .= $part[$x]." ";
                }
                else
                    $nama_pasien = $part[0];
                
                $pasien_exist = $this->backup_model->cek_pasien($nobase);
                if ($pasien_exist <= 0)
                {
                    $data_array[$i]['namacus'] = $nama_pasien;
                    $data_array[$i]['nobase'] = $nobase;
                    $data_array[$i]['alamat'] = $row->alamat;
                    $data_array[$i]['noax'] = $row->noax;
                    $data_array[$i]['kota'] = $row->kota;
                    $data_array[$i]['telp'] = $row->telp;
                    $data_array[$i]['id_cabang'] = $cabang;
    $data_array[$i]['dtgl'] = $row->awal;
                    $i++;
                }
            }
        }
        
        if (count($data_array) > 0)
            $this->upload_model->insert_pasien_ax($data_array);
            
        
		return $this->xmlrpc->send_response($response);
    }
    
    function index()
    {
//1 ----> SUCCESS
//0/other ---> error
        $user = $this->input->post('username');
        $pwd = $this->input->post('password');
        $jenis = $this->input->post('jenis');
        $data = $this->input->post('data');
        $cabang = $this->input->post('cabang');
        $tanggal = $this->input->post('tanggal');
        $tanggal = date("Y-m-d", strtotime($tanggal));
        $cabang = $this->upload_model->get_id_cabang($cabang);
        $ret = '1';
        
        $data_array = Array();
        $i = 0;
        if ($user == 'natasha' && $pwd == 'p1ssw0rd' && $cabang > 0)
        {
          switch($jenis)
          {
              case 'transaksi':
                  $data = json_decode($data);
                  foreach($data as $row)
                  {
                      if ($row->kodebrg != "")
                      {
                          $part = explode(" ", $row->nama);
                          $pos = count($part)-1;
                          $nobase = $part[$pos];
                          
                          $data_array[$i]['nobase'] = $nobase;
                          $data_array[$i]['nofak'] = $row->resep;
                          $data_array[$i]['kodebrg'] = $row->kodebrg;
                          $data_array[$i]['satuan'] = $row->satuan;
                          $data_array[$i]['qty'] = $row->qty;
                          $data_array[$i]['tanggal'] = $row->tanggal;
                          $data_array[$i]['id_cabang'] = $cabang;
                          $data_array[$i]['keterangan'] = $row->komen;
                          $data_array[$i]['keterangan_resep'] = $row->komen_resep;
                          $i++;
                          //$this->backup_model->buat_backup_transaksi($nobase, $row->resep, $row->kodebrg, $row->satuan, abs($row->qty), $tanggal, $cabang, $row->komen, $row->komen_resep);
                      }
                  }
                  
                  if (count($data_array) > 0)
                    $ret = $this->backup_model->buat_backup_transaksi($data_array, $cabang);
              break;
              
              case 'pasien':
                  $data = json_decode($data);
                  foreach($data as $row)
                  {
                      if ($row->nama != "")
                      {
                          $part = explode(" ", $row->nama);
                          $pos = count($part)-1;
                          $nobase = $part[$pos];
                          
                          $nama_pasien = "";                            
                          if ($pos > 1)
                          {
                              for($x=0; $x<=$pos-1; $x++)
                                  $nama_pasien .= $part[$x]." ";
                          }
                          else
                              $nama_pasien = $part[0];
                          
                          $pasien_exist = $this->backup_model->cek_pasien($nobase);
                          if ($pasien_exist <= 0)
                          {
                              $data_array[$i]['namacus'] = $nama_pasien;
                              $data_array[$i]['nobase'] = $nobase;
                              $data_array[$i]['alamat'] = $row->alamat;
                              $data_array[$i]['noax'] = $row->noax;
                              $data_array[$i]['kota'] = $row->kota;
                              $data_array[$i]['telp'] = $row->telp;
                              $data_array[$i]['id_cabang'] = $cabang;
        	                  $data_array[$i]['dtgl'] = $row->awal;
                              $i++;
                          }
                      }
                  }
                  
                  if (count($data_array) > 0)
                      $ret = $this->backup_model->buat_backup_pasien($data_array);
              break;
              
              case 'update':
                  $ret = $this->backup_model->update_tanggal_backup($cabang);
              break;
          }
        }
        else
            $ret = 'User, Password, atau kode cabang anda salah...';
        
        echo $ret;
 
    }
    
    
    function get_date($cabang = NULL)
    {
        if ($cabang != NULL)
        {
            $tanggal = $this->backup_model->ambil_tanggal($cabang);
            echo $tanggal;
        }
        else
            echo "0";
    }
    
}
