<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_ax extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('upload_model');
        $this->load->library('session');
        $this->load->library('Encrypt');
        
        $this->load->library('login_cek');
        $this->login_cek->is_login();
    }
    
    function index()
    {
        $data['msg'] = '';
        $data['cabang'] = $this->upload_model->get_cabang();
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('uploadAX', $data);
        $this->load->view('footer');
    }

//mm/dd/yyyy
    function convert_date($tanggal)//yyyy-mm-dd
    {
        $part = explode("/", $tanggal);
        
        if (count($part) == 3)
        {
            $date = $part[2]."-".$part[0]."-".$part[1];
            return date("Y-m-d", strtotime($date));
        }
        else
        {
            $date = '';
            return $date;
        }
            
    }

    function upload_ajax()
    {
        $data = $this->input->post('data');
        $id_source = $this->input->post('id_source');
        $tipe = $this->input->post('tipe');
        $cabang = $this->input->post('cabang');
        $reject = "";
        
        $row_data = Array();
        $pasien_baru = Array();
        $index = 0;
        switch($tipe)
        {
            case 'pasien':
                $row_pasien = Array();
                for($i = 0; $i <= count($data)-1; $i++)
                {
                    if (!empty($data[$i]) && !empty($id_source))
                    {
                        $item = explode("^", $data[$i]);
                        
                        if (!empty($item[0]))
                        {
                            $item_nama = explode(" ", $item[2]);
                            $posisi = count($item_nama) - 1;
                            $no_base = $item_nama[$posisi];
                            $nama_pasien = "";
                            
                            if ($posisi > 1)
                            {
                                for($x=0; $x<=$posisi-1; $x++)
                                    $nama_pasien .= $item_nama[$x]." ";
                            }
                            else
                                $nama_pasien = $item_nama[0];
                            
                            $pasien_exist = $this->upload_model->cek_pasien($no_base);

                            if ($item[9] == 'Female')
                                $gender = 'WANITA';
                            else
                                $gender = 'PRIA';
                            if ($pasien_exist <= 0)
                            {
                                $row_data[$index]['nobase'] = $no_base;
                                $row_data[$index]['noax'] = $item[1];
                                $row_data[$index]['namacus'] = $nama_pasien;
                                $row_data[$index]['telp'] = $item[6];
                                $row_data[$index]['id_source'] = $id_source;
                                $row_data[$index]['alamat'] = $item[5];
                                $row_data[$index]['tgllh'] = $this->convert_date($item[10]);//dd-mm-yyy
                                $row_data[$index]['dtgl'] = $this->convert_date($item[3]);//dd-mm-yyy
                                $row_data[$index]['sex'] = $gender;
                                $row_data[$index]['id_cabang'] = $cabang;
                                $index++; 
                                //$this->upload_model->insert_pasien_ax($no_base, $item[1], $nama_pasien, $item[2], $item[3], $id_source);
                            }
                            else
                            {
                                $this->upload_model->update_noax_detail($no_base, $item[1],$item[5], $this->convert_date($item[10]), $item[6], $gender);
                            }
                        }
                    }
                }
                if (count($row_data) > 0)
                    $this->upload_model->insert_pasien_ax($row_data);
                
            break;
            
            case 'transaksi':
                for($i = 0; $i <= count($data)-1; $i++)
                {
                    if (!empty($data[$i]) && !empty($id_source))
                    {
                        $item = explode("^", $data[$i]);
                        
                        if (!empty($item[3]))
                        {
                            $part_pasien = explode(" ", $item[3]);
                            $index_base = count($part_pasien)-1;
                            $nobase = $part_pasien[$index_base];
                            
                            $pasien_exist = $this->upload_model->cek_pasien($nobase);
                            if ($pasien_exist <= 0)
                            {
                                $nama_pasien = '';
                                $posisi = count($part_pasien)-1;
                                if ($posisi > 1)
                                {
                                    for($x=0; $x<=$posisi-1; $x++)
                                        $nama_pasien .= $part_pasien[$x]." ";
                                }
                                else
                                    $nama_pasien = $part_pasien[0];
                                
                                $pasien_baru[0]['nobase'] = $nobase;
                                $pasien_baru[0]['noax'] = $item[2];
                                $pasien_baru[0]['namacus'] = $nama_pasien;
                                
                                $this->upload_model->add_pasien($pasien_baru);
                            }
                            else
                                $this->upload_model->update_noax($nobase, $item[2]);
                                                           
                            
                            $row_data[$index]['nobase'] = $nobase;
                            $row_data[$index]['nofak'] = $item[4];
                            $row_data[$index]['noax'] = $item[2];
                            $row_data[$index]['kodebrg'] = $item[7];
                            $row_data[$index]['satuan'] = $item[11];
                            $row_data[$index]['qty'] = $item[10];
                            $row_data[$index]['harga'] = $item[14];
                            $row_data[$index]['discount'] = $item[13];
                            $row_data[$index]['tanggal'] = $this->convert_date($item[6]);//dd/mm/yyyy
                            $row_data[$index]['id_source'] = $id_source;
                            $row_data[$index]['id_cabang'] = $cabang;
                            $index++;
                        }
                    }
                }
                
                if (count($row_data) > 0)
                    $this->upload_model->insert_transaksi_ax($row_data);
            break;
            
        }
        
        echo $reject;
    }
}
