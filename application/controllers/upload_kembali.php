<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_kembali extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('upload_model');
        $this->load->library('session');
        $this->load->library('Encrypt');
        
        $this->load->library('login_cek');
        $this->login_cek->is_login();
    }
    
    function index()
    {
        $data['msg'] = '';
        $data['cabang'] = $this->upload_model->get_cabang();
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('upload_kembali', $data);
        $this->load->view('footer');
    }


    function upload_ajax()
    {
        $data = $this->input->post('data');
        $id_source = $this->input->post('id_source');
        $tipe = $this->input->post('tipe');
        //$cabang = $this->input->post('cabang');
        $reject = "";
        $tanggal = "";
        $cabang_transaksi = "";
        $no_base = "";
        
        
        $row_data = Array();
        switch($tipe)
        {
            case 'balik':
                $row_pasien = Array();
                $index = 0;
                for($i = 0; $i <= count($data)-1; $i++)
                {
                    if (!empty($data[$i]) && !empty($id_source))
                    {
                        $item = explode("^", $data[$i]);
                        
                        if (!empty($item[2]))
                        {
                            $no_base = $item[0];
                            $cabang_transaksi = $this->upload_model->get_id_cabang($item[4]);
                            $kode_brg = $item[2];
                            $qty = empty($item[3]) ? 1 : $item[3];
                            $tggl = $this->convert_date($item[5]);
                            $keterangan = $item[6];
                            
                            
                            $this->upload_model->insert_data_kembali($no_base, $kode_brg, $qty, $tggl,$id_source,$cabang_transaksi, $keterangan);
                        }
                    }
                }
                
            break;            
        }
        
        echo $reject;
    }
    
    
    function convert_date($tanggal)
    {
        $part = explode("/", $tanggal);
        
        if (count($part) == 3)
        {
            $date = $part[2]."-".$part[1]."-".$part[0];
            return date("Y-m-d", strtotime($date));
        }
        else
        {
            $date = '';
            return $date;
        }
            
    }
}
