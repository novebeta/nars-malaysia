<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Encrypt');
    }
    
    function index()
    {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('footer');
    }
}