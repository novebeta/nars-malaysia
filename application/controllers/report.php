<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('report_model');
        $this->load->library('session');
        $this->load->library('Encrypt');
        
        $this->load->library('login_cek');
        $this->login_cek->is_login();
    }
    
    function index()
    {
        $data['report'] = '';
        $data['header'] = '';
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('report_list', $data);
        $this->load->view('footer');
    }
    
    function create_report()
    {
        $awal = $this->input->post('awal');
        $akhir = $this->input->post('akhir');
        //$tipe = $this->input->post('tipe');
        
        $cabang = $this->session->userdata('cabang');
        $kode_cabang = $this->report_model->get_kode_cbg($cabang);

        $data['report'] = '1';
        $data['perawatan'] = '';
        $data['baru'] = '';
        $data['riil'] = '';
        
                
        $run = $this->report_model->get_perawatan($awal, $akhir,$cabang);
        $namacus = '';
            
        foreach($run->result() as $row)
        {
            $data['perawatan'] .= '<tr>
                <td>'.$row->nobase.'</td>
                <td>'.$row->namacus.'</td>
                <td>'.$row->kodebrg.'</td>
                <td>'.$row->tanggal.'</td>
            </tr>';
        }
        
        $run = $this->report_model->get_pasien_baru($awal, $akhir,$kode_cabang);
        
        foreach($run->result() as $row)
        {
            $data['baru'] .= '<tr>
                <td>'.$row->nobase.'</td>
                <td>'.$row->namacus.'</td>
                <td>'.$row->dtgl.'</td>
            </tr>';
        }
        
        $run = $this->report_model->pasien_riil($awal, $akhir,$cabang);
        
        foreach($run->result() as $row)
        {
            $data['riil'] .= '<tr>
                <td>'.$row->nobase.'</td>
                <td>'.$row->namacus.'</td>
                <td>'.$row->tanggal.'</td>
            </tr>';
        }
        
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('report_list', $data);
        $this->load->view('footer');
    }
    
}
