<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uploaded extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('uploaded_model');
        $this->load->library('session');
        $this->load->library('Encrypt');
        $this->load->library('pagination');
        
        $this->load->library('login_cek');
        $this->login_cek->is_login();
    }
    
    function index()
    {
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('uploaded_view');
        $this->load->view('footer');
    }
    
    function hapus()
    {
        $id_source = $this->input->post('id');
        $tipe = $this->input->post('type');
        
        $data = $this->uploaded_model->hapus_data($id_source, $tipe);
        
        echo $data;
    }
    
    function nsc01()
    {
        $config = array();
        
        $config["base_url"] = base_url() . "uploaded/nsc01";
        
        $config["total_rows"] = $this->uploaded_model->nsc01_count();
        $config["per_page"] = per_page;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        $list = $this->uploaded_model->nsc01_data($page, $config["per_page"]);
        if ($list->num_rows() > 0)
            $data["results"] = $list;//->result();
        else
            $data["results"] = array();
        $data["links"] = $this->pagination->create_links(); 
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('uploaded_view', $data);
        $this->load->view('footer');
    }
    
    
    function nsc08()
    {
        $config = array();
        
        $config["base_url"] = base_url() . "uploaded/nsc08";
        
        $config["total_rows"] = $this->uploaded_model->nsc08_count();
        $config["per_page"] = per_page;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        $list = $this->uploaded_model->nsc08_data($page, $config["per_page"]);
        if ($list->num_rows() > 0)
            $data["results"] = $list;//->result();
        else
            $data["results"] = array();
        $data["links"] = $this->pagination->create_links(); 
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('uploaded_view', $data);
        $this->load->view('footer');
    }
    
    
    function nsc09()
    {
        $config = array();
        
        $config["base_url"] = base_url() . "uploaded/nsc09";
        
        $config["total_rows"] = $this->uploaded_model->nsc09_count();
        $config["per_page"] = per_page;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        
        $list = $this->uploaded_model->nsc09_data($page, $config["per_page"]);
        if ($list->num_rows() > 0)
            $data["results"] = $list;//->result();
        else
            $data["results"] = array();
        $data["links"] = $this->pagination->create_links(); 
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('uploaded_view', $data);
        $this->load->view('footer');
    }
}