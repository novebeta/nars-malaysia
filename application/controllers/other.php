<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Other extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Encrypt');
        $this->load->library('pagination');
        
        $this->load->library('login_cek');
        $this->login_cek->is_login();
    }


    function index()
    {
        $id_user = $this->session->userdata('id_user');
        $nama = $this->session->userdata('nama');
        $level = $this->session->userdata('level');
        $expired = $this->session->userdata('expired');
        
        redirect('http://180.250.148.165:777/mobile/shortcut/go/'.$id_user.'/'.$nama.'/'.$expired);
    }
    
}
