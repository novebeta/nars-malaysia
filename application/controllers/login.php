<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->library('session');
        $this->load->library('Encrypt');
    }
	
    public function index()
	{
        $data['msg'] = $this->session->userdata('expired');
	if ($this->session->userdata('id_user'))
		redirect(base_url().'summary');

	$this->load->view('header');
		$this->load->view('login_view', $data);
        $this->load->view('footer');
	}
    
    function masuk()
    {
        $user = $this->input->post('user');
        $pwd = $this->input->post('pwd');
        
        $ok = $this->login_model->cek_masuk($user, $pwd);
        if ($ok == true)
        {
            redirect(base_url().'summary');
        }
        else
        {
            $data['msg'] = 'Password atau username anda salah';
            $this->load->view('header');
    		$this->load->view('login_view', $data);
            $this->load->view('footer');
        }
    }
    
    function keluar()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
