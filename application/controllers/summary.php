<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Summary extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('summary_model');
        $this->load->library('session');
        $this->load->library('Encrypt');
        $this->load->library('pagination');
        
        $this->load->library('login_cek');
        $this->login_cek->is_login();
    }


    function index()
    {
        $data['msg'] = '';
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('summary_view', $data);
        $this->load->view('footer');
    }
    
    
    function find($f, $nama, $alamat, $telp, $no_base, $page = 0)
    {
        //$nama = $this->input->post('nama');
        //$tanggal = $this->input->post('tanggal');
        //$no_base = $this->input->post('no_base');
        $nama = urldecode($nama);
        $alamat = urldecode($alamat);
        $no_base = urldecode($no_base);
        $telp = urldecode($telp);
        $config = array();
        
        $this->session->set_userdata('nama_pasien', $nama);
        $this->session->set_userdata('alamat', $alamat);
        $this->session->set_userdata('no_base', $no_base);
        $this->session->set_userdata('telp', $telp);
        $this->session->set_userdata('page', $page);
        $this->session->set_userdata('f', $f);
        
        
        $config["base_url"] = base_url() . "summary/find/$f/$nama/$alamat/$telp/$no_base";
        if ($nama == '-')
            $nama = null;
        if ($alamat == '-')
            $alamat = null;
        if ($telp == '-')
            $telp = null;
        if ($no_base == '-')
            $no_base = null;
                
        
        $config["total_rows"] = $this->summary_model->find_count($nama, $alamat, $telp, $no_base, $f);
        $config["per_page"] = per_page;
        $config["uri_segment"] = 8;

        $this->pagination->initialize($config);
        //$page = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
        
        $list = $this->summary_model->find_data($nama, $alamat, $telp, $no_base, $f, $page, $config["per_page"]);
        if ($list->num_rows() > 0)
            $data["results"] = $list;//->result();
        else
            $data["results"] = array();
        $data["links"] = $this->pagination->create_links(); 
        
        
        
        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('summary_view', $data);
        $this->load->view('footer');
        
    }
    
    
    function detail($no_base)
    {
        $data["info"] = $this->summary_model->info_pasien($no_base);
        $data["results"] = $this->summary_model->detail_pasien_all($no_base);

        $this->load->view('header');
        $this->load->view('menu');
        $this->load->view('summary_detail_view', $data);
        $this->load->view('footer');
    }
}
