<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class login_cek 
{

    function __construct() 
    {
       $this->ci =& get_instance();
       $this->ci->load->library('session');
    }

    function is_login()
    {
        $user = $this->ci->session->userdata('id_user');
        
        if (empty($user))
        {
            $this->ci->session->sess_destroy();
            redirect(base_url());
        }
        else
        {
            $expired = $this->ci->session->userdata('expired');
            $now = time();
            
            if(empty($expired) || $expired < $now)
            {
                $this->ci->session->sess_destroy();
                redirect(base_url());
            }
            else
            {
                $expired = $now + waktu_login;
                $this->ci->session->set_userdata('expired', $expired);
            }
        }
    }
}