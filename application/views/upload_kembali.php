
<script type='text/javascript' src='<?php echo base_url(); ?>js/xls/xls.js'></script>

<div class="application">

    <div class="label">Browse File</div> : <input type="file" name="userfile" id="userfile" /> <br />
    <!-- BUG: <div class="label">Cabang Transaksi</div> : 
        <select name="cabang" id="cabang">
            <?=$cabang?>
        </select>
    <br /> -->
<input type="button" name="jread" id="jread" value="UPLOAD" onclick="baca()" />
    <?=$msg?>
<?=form_close()?> 
    <div id="proses"></div><br />
    <div id="reject"></div>
</div>


<script>

    
    function xlsworker(data, cb) 
    {
        var worker = new Worker('<?=base_url()?>js/xls/xlsworker.js');
        worker.onmessage = function(e) 
        {
            switch(e.data.t) 
            {
                case 'ready': break;
                case 'e': console.error(e.data.d); break;
                case 'xls': cb(e.data.d); break;
            }
        };
	   worker.postMessage(data);
    }


    function baca()
    {
        var files = document.getElementById('userfile').files;
        if (!files.length) {
          alert('Please select a file!');
          return;
        }
        else
        {
            $('#proses').html('Proses : Membaca File...');
           	var i,f;
            for (i = 0, f = files[i]; i != files.length; ++i) 
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = function(e) 
                {
                    var data = e.target.result;
                    if(typeof Worker !== 'undefined') 
                    {
                        xlsworker(data, process_wb);
                    } 
                    else 
                    {
                        var wb = XLS.read(data, {type:'binary'});
				        process_wb(wb);
                    }
                };
		          reader.readAsBinaryString(f);
            }
        }   
    }
    
    
    function process_wb(wb) 
    {
        /*var output = "";
        var arr= new Array();
        arr = to_json(wb);
        
            alert(JSON.stringify(arr.Sheet1));*/
            
        to_csv(wb);
    }
    
    
    function to_json(workbook) 
    {
        var result = [];
        workbook.SheetNames.forEach(function(sheetName) 
        {
            var opt = {FS:"^"};
            var roa = XLS.utils.make_csv(workbook.Sheets[sheetName], opt);
            if(roa.length > 0)
            {
                result[sheetName] = roa;
            }
        });
        return result;
    }
    
    function to_csv(workbook) 
    {
        var result = [];
        var i = 0;
        workbook.SheetNames.forEach(function(sheetName) 
        {
            //if (i == 0)
            //{
                var opt = {FS:"^"};
                var csv = XLS.utils.make_csv(workbook.Sheets[sheetName], opt);
                if(csv.length > 0)
                {
                    //alert(csv);
                
                    var arr = csv.split("\n");
                    var header = arr[18].split('^');
                    if (header[4].search("CABANG TRANSAKSI") == -1)
                    {
                        alert("Bukan file PENGEMBALIAN DATA");
                        return false;
                    }
                    else
                    {
		            var ret_val = cek_data(arr); 
		            if (ret_val > 0)
		                alert("File yang anda upload error pada line "+(ret_val+1)+
		                    "\nSilahkan cek lagi file tersebut");
		            else
		            {
		                $.ajax({
		                    url     : '<?=base_url()?>upload_sin/create_source',
		                    data    : {tipe : 'balik', path : $('#userfile').val()},
		                    type    : 'POST',
		                    async   : false,
		                    success : function(hasil){
		                        if (hasil > 0)
		                            upload_data(arr, hasil);
		                        else
		                        {
		                            alert('Gagal melakukan upload data');
		                            return false;
		                        }
		                    }
		                });
		                alert('Proses Upload telah selesai');
		            }
			}
                }
                else
                {
                    alert('File kosong atau tidak bisa dibaca');
                }
                    
            //}
            i++;   
        });
    }
    
    function cek_data(data_arr)
    {
        var mulai_baca = 20-1;
        for(var i=mulai_baca; i<=data_arr.length; i++)
        {
            if (data_arr[i] !== undefined)
            {
                var kolom = data_arr[i].replace(/ /g, '');
                kolom = kolom.split("^");
                
                if (kolom[7] !== undefined)
                {
                    if (kolom[7].search("VALUE") > 0)
                        return i;
                    else
                    {
                        if (kolom[2] != '')
                        {
                            if (kolom[0] == '' || kolom[4] == '' || kolom[5] == '')
                                return i;
                        }
                    }
                }
            }
        }
        return 0;
    }
    
    function upload_data(arrx, source)
    {
        //var arr = datane.split("\n");
        var arr = arrx.slice(18, arrx.length);
        var total = arr.length;
        var loop = 30;//<=========================EDIT INI SAJA
        var awal = 1;
        var adder = Math.ceil(total/loop);
        var akhir = adder;
        var too_small = false;
        
        var proc = 100/loop;
        var percent=0;
        
        for(var i=1; i<=loop; i++)
        {
            percent += proc;
            $('#proses').html('Proses : Upload data.... ('+percent+'%)');
            if (akhir == awal)
            {
                akhir++;
                too_small = true;
            }
            if (akhir > total || awal > total)
            {
                akhir = total;
                i = loop;
            } 
            var temp = arr.slice(awal, akhir);
            
            //alert(awal+'ok'+akhir);
            $.ajax({
                    url     : '<?=base_url()?>upload_kembali/upload_ajax',
                    data    : {data : temp, id_source : source, tipe : 'balik'},
                    type    : 'POST',
                    async   : false,
                    success : function(hasil){
                        if (hasil.length > 10)
                        {
                            var inn = document.getElementById('reject').innerHTML;
                            
                            document.getElementById('reject').innerHTML = inn + hasil;
                        }
                    }
                });
            
            awal = akhir;
            akhir = akhir + adder;
        }
        
        $('#proses').html('Proses : SELESAI...');
    }
    
    
</script>
