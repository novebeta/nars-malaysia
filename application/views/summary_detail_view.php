

<div class="application">
    <input type="button" name="back" id="back" value="BACK" onclick="depan()" />
    <br />
    <div class="left half">
        <div class="label">Name</div> : <?=$info->namacus?> <br />
        <div class="label">Address</div> : <?=$info->alamat." ".$info->kota?> <br />
        <div class="label">Phone No.</div> : <?=$info->telp?>
        
    </div>
<br />
<? 
    //echo $links;
    ?>
    <br />
    <br />
    <table>
    <thead>
    <tr>
        <th>No</th>
        <th>Receipt No.</th>
        <th>Item Code</th>
        <th>Qty</th>
        <th>Unit</th>
        <th>Transaction Date</th>
        <th>Transaction Location</th>
        <th>Note</th>
        <th>Comment</th>
    </tr>
    </thead>
    
    <tbody>
    <? if ($results->num_rows() > 0)
    {
        $i = 0;
        
        $nofak = '';
        $page = ($this->uri->segment(4))? 0 : $this->uri->segment(4);
        foreach ($results->result() as $row)
        {
            
            echo '<tr>';
            if ($nofak != $row->nofak)
            {
                $i++;
                $no = $i + $page;
                $nofak = $row->nofak;
                echo '<td>'.$no.'</td>';
                echo '<td>'.$row->nofak.'</td>';
            }
            else
            {
                echo '<td></td>';
                echo '<td></td>';
            }
            
            echo '<td>'.$row->kodebrg.'</td>';
            echo '<td>'.$row->qty.'</td>';
            echo '<td>'.$row->satuan.'</td>';
            echo '<td>'.date("d-M-Y", strtotime($row->tanggal)).'</td>';
            echo '<td>'.$row->kode_cabang.'</td>';
            echo '<td>'.$row->keterangan.'</td>';
            echo '<td>'.$row->keterangan_resep.'</td>';
            echo '</tr>';
        }
        
    }
else
{
	echo '<tr>
	<td colspan="9" style="text-align:center">NO RECORD</td>
	</tr>';
} ?>
    </tbody>
    </table>
    <br />
<? //echo $links;
     ?>
</div>

<script>
    
    function depan()
    {
        window.location = '<?=base_url()?>summary/find/<?=$this->session->userdata('f')?>/<?=$this->session->userdata('nama_pasien')?>/<?=$this->session->userdata('alamat')?>/<?=$this->session->userdata('telp')?>/<?=$this->session->userdata('no_base')?>/<?=$this->session->userdata('page')?>';
    }
</script>
