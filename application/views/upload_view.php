
<script type='text/javascript' src='<?php echo base_url(); ?>js/xlsx/xlsx.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/xlsx/jszip.js'></script>

<div class="application">

<?=form_open_multipart(base_url().'upload_sin/upload_file')?>
    <div class="label">Brows File</div> : <input type="file" name="userfile" id="userfile" /> <br />
    <div class="label">File type</div> : 
        <select id="tipe" name="tipe">
            <option value="nsc01">NSC01</option>
            <option value="nsc08">NSC08</option>
            <option value="nsc09">NSC09</option>
        </select>
    <br />
    <div class="label">Cabang</div> : 
        <select name="cabang" id="cabang">
        <?=$cabang?>
        </select>
    <br />
    <!-- BUG: <input type="submit" id="submit" name="submit" value="upload" /> -->
<input type="button" name="jread" id="jread" value="UPLOAD" onclick="baca()" />
    <?=$msg?>
<?=form_close()?> 
Pastikan anda telah menrubah file <strong>DBF</strong> menjadi <strong>XLSX</strong> dan <strong>merubah format semua tanggal</strong>
    <div id="proses"></div><br />
    <div id="reject"></div>
</div>


<script>

    var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
    function fixdata(data) 
    {
        var o = "", l = 0, w = 10240;
        for(; l<data.byteLength/w; ++l)
        o+=String.fromCharCode.apply(null,new Uint8Array(data.slice(l*w,l*w+w)));
        o+=String.fromCharCode.apply(null, new Uint8Array(data.slice(o.length)));
        return o;
    }
    
    function xlsxworker(data, cb) 
    {
        var worker = new Worker('<?=base_url()?>js/xlsx/xlsxworker.js');
        worker.onmessage = function(e) 
        {
            switch(e.data.t) 
            {
                case 'ready': break;
                case 'e': console.error(e.data.d); break;
                case 'xlsx': cb(JSON.parse(e.data.d)); break;
            }
        };
        var arr = rABS ? data : btoa(fixdata(data));
        worker.postMessage({d:arr,b:rABS});
    }


    function baca()
    {
        var files = document.getElementById('userfile').files;
        if (!files.length) {
          alert('Please select a file!');
          return;
        }
        else
        {
            $('#proses').html('Proses : Membaca File...');
           	var i,f;
            for (i = 0, f = files[i]; i != files.length; ++i) 
            {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = function(e) 
                {
                    var data = e.target.result;
                    if(typeof Worker !== 'undefined') 
                    {
                        xlsxworker(data, process_wb);
                    } 
                    else 
                    {
                        var wb;
                        if(rABS) 
                        {
                            wb = XLSX.read(data, {type: 'binary'});
                        } 
                        else 
                        {
                            var arr = fixdata(data);
                            wb = XLSX.read(btoa(arr), {type: 'base64'});
                        }
                        process_wb(wb);
                    }
                };
                if(rABS) reader.readAsBinaryString(f);
                else reader.readAsArrayBuffer(f);
            }
        }   
    }
    
    
    function process_wb(wb) 
    {
        /*var output = "";
        var arr= new Array();
        arr = to_json(wb);
        
        output = JSON.stringify(arr, 2, 2); //JSON
        if(out.innerText === undefined) out.textContent = output;
        else out.innerText = output;
            alert(JSON.stringify(arr.Sheet1[3]));*/
            
            
        //output = to_csv(wb);
        to_csv(wb);
    }
    
    
    function to_json(workbook) 
    {
        var result = {};
        workbook.SheetNames.forEach(function(sheetName) {
            var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            if(roa.length > 0)
            {
                result[sheetName] = roa;
            }
        });
        return result;
    }
    
    function to_csv(workbook) 
    {
        var result = [];
        workbook.SheetNames.forEach(function(sheetName) 
        {
            var opt = {FS:"^"};
            var csv = XLSX.utils.sheet_to_csv(workbook.Sheets[sheetName], opt);
            if(csv.length > 0)
            {
                //alert(csv);
            
                var tipe = $('#tipe').val();
                
                switch (tipe)
                {
                    case 'nsc01':
                        var arr = csv.split("\n");
                        var header = arr[0].split('^');
                        if (header[1].search("NOBASE") == -1)
                        {
                            alert("Bukan NSC01");
                            return false;
                        }
                    break;
                    
                    case 'nsc08':
                        var arr = csv.split("\n");
                        var header = arr[0].split('^');
                        if (header[1].search("NOFAK") == -1)
                        {
                            alert("Bukan NSC08");
                            return false;
                        }
                    break;
                    
                    case 'nsc09':
                        var arr = csv.split("\n");
                        var header = arr[0].split('^');
                        if (header[3].search("KODEBRG") == -1)
                        {
                            alert("Bukan NSC09");
                            return false;
                        }
                    break;
                }
                
                $.ajax({
                        url     : '<?=base_url()?>upload_sin/create_source',
                        data    : {tipe : $('#tipe').val(), path : $('#userfile').val(), cabang : $('#cabang').val()},
                        type    : 'POST',
                        async   : false,
                        success : function(hasil){
                            if (hasil > 0)
                                upload_data(arr, hasil);
                            else
                            {
                                alert('Gagal melakukan upload data');
                                return false;
                            }
                        }
                    });
                
            }
            else
            {
                alert('File kosong atau tidak bisa dibaca');
            }
                
            alert('Proses Upload telah selesai');
                
        });
    }
    
    function upload_data(arr, source)
    {
        //var arr = datane.split("\n");
        var total = arr.length;
        var loop = 30;//<=========================EDIT INI SAJA
        var awal = 1;
        var adder = Math.ceil(total/loop);
        var akhir = adder;
        var too_small = false;
        
        var proc = 100/loop;
        var percent=0;
        
        for(var i=1; i<=loop; i++)
        {
            percent += proc;
            $('#proses').html('Proses : Upload daata.... ('+percent+'%)');
            if (akhir == awal)
            {
                akhir++;
                too_small = true;
            }
            if (akhir > total || awal > total)
            {
                akhir = total;
                i = loop;
            } 
            var temp = arr.slice(awal, akhir);
            
            //alert(awal+'ok'+akhir);
            $.ajax({
                    url     : '<?=base_url()?>upload_sin/upload_ajax',
                    data    : {data : temp, id_source : source, tipe : $('#tipe').val(), cabang : $('#cabang').val()},
                    type    : 'POST',
                    async   : false,
                    success : function(hasil){
                        if (hasil.length > 10)
                        {
                            var inn = document.getElementById('reject').innerHTML;
                            
                            document.getElementById('reject').innerHTML = inn + hasil;
                        }
                    }
                });
            
            awal = akhir;
            akhir = akhir + adder;
        }
        
        $('#proses').html('Proses : SELESAI...');
    }
    
    
</script>
