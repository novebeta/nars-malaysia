

<div class="application">
    <div class="half left">
        <div class="label">Customer Name</div> <input type="text" name="nama" id="nama"  /> <br />
        <div class="label">Address</div> <input type="text" name="alamat" id="alamat"  /> <br />
    </div>
    <div class="half left">
        <div class="label">Phone No.</div> <input type="text" name="telp" id="telp"  /> <br />
        <div class="label">Customer No.</div> <input type="text" name="no_base" id="no_base"  /> <br />
    </div>
    <br />
    <div class="full left">
        <input type="checkbox" name="kiri" id="kiri" checked="checked" /><label for="kiri">Search by First Name</label>
    </div>
    <br />
    <br />
    <input type="button" onclick="cari()" id="submit" name="submit" value="Search" />
    <br />
    <br />
<? if ($this->uri->segment(3) == 'f' || $this->uri->segment(3) == 'k') { 
    echo $links;
    ?>
    <br />
    <table>
    <thead>
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Customer No.</th>
        <th>Address</th>
        <th>City</th>
        <th>Phone</th>
        <th>Detail</th>
    </tr>
    </thead>
    
    <tbody>
    <? if (count($results)>0)
    {
        $i = 0;
	$page = $this->uri->segment(8);
        $page = empty($page) ? 0 : $this->uri->segment(8);
        foreach ($results->result() as $row)
        {
            $i++;
        $no = $i + $page;
            echo '<tr>';
            echo '<td>'.$no.'</td>';
            echo '<td>'.$row->namacus.'</td>';
            echo '<td>'.$row->nobase.'</td>';
            echo '<td>'.$row->alamat.'</td>';
            echo '<td>'.$row->kota.'</td>';
            echo '<td>'.$row->telp.'</td>';
            echo '<td><i class="fa fa-folder-open-o fa-lg" onclick="detail(\''.$row->nobase.'\')"</i></td>';
            echo '</tr>';
        }
        
    } ?>
    </tbody>
    </table>
    <br />
<? echo $links;
    } ?>
</div>

<script>
    $(document).ready(function(){
        $('#nama').focus();
        
        $('#nama').keydown(function(e){
    		var charCode = (e.which) ? e.which : e.keyCode;

            if (charCode == 13 || charCode == 9)
            {
                $('#alamat').focus();
            }
        });
        
        $('#alamat').keydown(function(e){
    		var charCode = (e.which) ? e.which : e.keyCode;

            if (charCode == 13 || charCode == 9)
            {
                $('#telp').focus();
            }
        });
        
        $('#telp').keydown(function(e){
    		var charCode = (e.which) ? e.which : e.keyCode;

            if (charCode == 13 || charCode == 9)
            {
                $('#no_base').focus();
            }
        });
        
        $('#no_base').keydown(function(e){
    		var charCode = (e.which) ? e.which : e.keyCode;

            if (charCode == 13 || charCode == 9)
            {
                cari();
            }
        });
    });
    
    function cari()
    {
        var nama = '-';
        var alamat = '-';
        var telp = '-';
        var no_base = '-';
        
        if ($('#nama').val() != '')
            nama = $('#nama').val();
        if ($('#alamat').val() != '')
            alamat = $('#alamat').val();
        if ($('#telp').val() != '')
            telp = $('#telp').val();
        if ($('#no_base').val() != '')
            no_base = $('#no_base').val();
        
        if (nama == '-' && alamat == '-' && telp == '-' && no_base == '-')
        {
            alert('Anda tidak diperbolehkan mengosongkan data pencarian');
            return false;
        }
        
        var kiri ='f';    
        if ($('#kiri').is(':checked'))
            kiri = 'k';
        
        window.location = '<?=base_url()?>summary/find/'+kiri+'/'+nama+'/'+alamat+'/'+telp+'/'+no_base+'/0';
    }
    
    function detail(base, kartu)
    {
        window.location = '<?=base_url()?>summary/detail/'+base;
    }
</script>
