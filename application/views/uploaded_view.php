


<div class="application">
    <div class="label">File Type</div>
        <select id="tipe" name="tipe">
            <option value="0">Tipe</option>
            <option value="nsc01">NS01</option>
            <option value="nsc08">NS08</option>
            <option value="nsc09">NS09</option>
        </select>
        <br />
    <br />
    <br />

<?

    if (isset($results))
    {
?>
    <table>
    <thead>
    <tr>
        <th>No</th>
        <th>Tipe</th>
        <th>Cabang</th>
        <th>Upload Path</th>
        <th>Delete</th>
    </tr>
    </thead>
    
    <tbody>
<?
    if (!empty($results))
    {
        $page = $this->uri->segment(3);
        $tipe = $this->uri->segment(2);
        $page = empty($page)? 0: $page;
        $i=0;
        foreach($results->result() as $row)
        {
            $i++;
            $no = $page + $i;
            echo '<tr>';
            echo '<td>'.$no.'</td>';
            echo '<td>'.$row->tipe.'</td>';
            echo '<td>'.$row->source.'</td>';
            echo '<td>'.$row->kode_cabang.'</td>';
            echo '<td><i class="fa fa-trash-o fa-lg" onclick="hapus('.$row->id_source.', \''.$tipe.'\')"></i></td>';
            echo '</tr>';
        }
    }
    
?>
    </tbody>
    </table>
    <br />
<? echo $links;
    }
?>
</div>


<script>
    $(document).ready(function() { 
        $('#tipe').change(function(){
            var tipe = $('#tipe').val();
            
            if (tipe != '0')
                window.location = '<?=base_url()?>uploaded/'+tipe;
        });
        
        
    });
    
    function hapus(id_source, tipe)
    {
        $.ajax({
            url     : '<?=base_url()?>uploaded/hapus',
            data    : {id : id_source, type : tipe},
            type    : 'POST',
            success : function(hasil){
                if (hasil > 0)
                    location.reload();
                else
                    alert('Gagal Menghapus Data');
            }
        });
    }
</script>