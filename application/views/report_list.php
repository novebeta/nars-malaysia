



<div class="application">
    <?=form_open(base_url().'report/create_report')?>
    <!-- BUG: <div class="label">Report</div>: 
        <select id="tipe" name="tipe">
            <option value="perawatan">Perawatan</option>
            <option value="baru">Pasien Baru</option>
            <option value="riil">Pasien Riil</option>
        </select><br /> -->
    <div class="label">Tanggal Awal </div>: <input type="text" name="awal" id="awal" /> <br />
    <div class="label">Tanggal Akhir</div>: <input type="text" name="akhir" id="akhir" /> <br />
    <br />
    <br />
    <input type="submit" id="submit" name="submit" value="Proses" />
    <?=form_close()?>
    

<?

    if ($report == 1)
    {
?>
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">Pasien Perawatan</a></li>
            <li><a href="#tabs-2">Pasien Baru</a></li>
            <li><a href="#tabs-3">Pasien Riil</a></li>
        </ul>
        
        <div id="tabs-1">
            <table class="full">
            <thead>
            <tr>
                <th>No Base</th>
                <th>Nama</th>
                <th>Kode Barang</th>
                <th>Tanggal</th>
            </tr>
            </thead>
            
            <tbody>
            <?=$perawatan?>
            </tbody>
            </table>
        </div>
        
        
        <div id="tabs-2">
            <table class="full">
            <thead>
            <tr>
                <th>No Base</th>
                <th>Nama</th>
                <th>Tanggal</th>
            </tr>
            </thead>
            
            <tbody>
            <?=$baru?>
            </tbody>
            </table>
        </div>
        
        <div id="tabs-3">
            <table class="full">
            <thead>
            <tr>
                <th>No Base</th>
                <th>Nama</th>
                <th>Tanggal</th>
            </tr>
            </thead>
            
            <tbody>
            <?=$riil?>
            </tbody>
            </table>
        </div>
    </div>
<?
}
?>
</div>

<script>
    
    $(function() {
        $( "#tabs" ).tabs();
    }); 
    
    $('#awal').datepicker({
        dateFormat: 'yy-mm-dd',
        onSelect : function(tggl) {
            var min = new Date(tggl);
            $('#akhir').datepicker('option', 'minDate', min);
        }
    });
    
    $('#akhir').datepicker({
        dateFormat: 'yy-mm-dd'
    })
</script>
